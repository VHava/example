<?php

namespace App\Parsers;

use App\Entity\User\User;
use App\Services\Cms\CsvParseIterator;
use App\Services\Notification\NotificationListeners;
use App\Services\Notification\NotificationService;
use App\Services\Notification\PlatformLogService;
use App\Services\Offer\MemberApplicationNotifier;
use App\Traits\YieldTrait;
use Doctrine\ORM\EntityManager;
use App\Encryptor\EncryptorService;
use App\Entity\Offer\BankStatement;
use App\Entity\Offer\MemberApplication;
use App\Entity\Offer\Offer;
use App\Repository\BankStatementRepository;
use FOS\UserBundle\Util\TokenGenerator;

/**
 * Class BankStatementCSVParser
 * @package App\Parsers
 */
class BankStatementCSVParser
{
    use YieldTrait;
    
    public static $CSV_HEADERS = ['date', 'amount', 'text'];

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var NotificationService
     */
    private $notification;

    /**
     * @var PlatformLogService
     */
    private $platformLogService;

    /**
     * @var EncryptorService
     */
    private $encryptor;

    /**
     * @var MemberApplicationNotifier
     */
    private $memberApplicationNotifier;

    /**
     * BankStatementCSVParser constructor.
     * @param EntityManager $entityManager
     * @param EncryptorService $encryptor
     * @param NotificationService $notification
     * @param PlatformLogService $platformLogService
     * @param MemberApplicationNotifier $memberApplicationNotifier
     */
    public function __construct(
        EntityManager $entityManager,
        EncryptorService $encryptor,
        NotificationService $notification,
        PlatformLogService $platformLogService,
        MemberApplicationNotifier $memberApplicationNotifier
    ) {
        $this->em = $entityManager;
        $this->encryptor = $encryptor;
        $this->notification = $notification;
        $this->platformLogService = $platformLogService;
        $this->memberApplicationNotifier = $memberApplicationNotifier;
    }

    /**
     * @return BankStatementRepository
     */
    public function getRepository()
    {
        return $this->getManager()->getRepository(BankStatement::class);
    }

    /**
     * @return NotificationService
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @return EntityManager
     */
    public function getManager()
    {
        return $this->em;
    }

    /**
     * @return PlatformLogService
     */
    public function getPlatformLogService()
    {
        return $this->platformLogService;
    }

    /**
     * @return MemberApplicationNotifier
     */
    public function getMemberApplicationNotifier()
    {
        return $this->memberApplicationNotifier;
    }

    /**
     * @param $path
     * @param $importFields
     * @param bool $sendNotifications
     * @return array
     */
    public function parseCSV($path, $importFields, $sendNotifications = false)
    {
        $parsedData = [];

        $csv = new CsvParseIterator($path, $importFields);

        $referenceNumbers = $this->getManager()->getRepository(MemberApplication::class)->getMAReferenceNumber();

        if (empty($referenceNumbers)) {
            if ($sendNotifications) {
                $this->getPlatformLogService()->create(
                    'Reference numbers of Member Applications are empty',
                    'Reference numbers of Member Applications are empty'
                );
            }

            return $parsedData;
        }

        $referenceNumbers = $this->encryptor->decryptArray($referenceNumbers);

        $row = 1;

        foreach ($csv->parse() as $record) {
            $row++;

            $record = array_change_key_case($record, CASE_LOWER);

            $missing = array_diff_key(array_flip($importFields), $record);

            if (count($missing) > 0) {
                if ($sendNotifications) {
                    $this->getPlatformLogService()->create(
                        'Required fields are missed',
                        sprintf(
                            'Please, check required fields in file',
                            implode(',', $importFields)
                        )
                    );
                }
                continue;
            }

            $searchMaField = 'text';

            if (!isset($record[$searchMaField])) {
                if ($sendNotifications) {
                    $this->getPlatformLogService()->create(
                        sprintf('Can not found field %s in csv row ', $searchMaField),
                        sprintf('Row must contain field %s. Row %s', $searchMaField, $row)
                    );
                }

                continue;
            }

            $maData = null;

            foreach ($referenceNumbers as $referenceNumber) {
                if (stripos($record[$searchMaField], $referenceNumber['referenceNumber']) !== false) {
                    $referenceNumber['clear_status'] = MemberApplication::convertMAStatus($referenceNumber['status']);
                    $maData = $referenceNumber;
                    break;
                }
            }

            $record["uniqueid"] = (new TokenGenerator())->generateToken();

            if ($maData === null) {
                $parsedData["not_found"][] = $record;
            } else {
                $bankStatement = $this->checkPreviousBankStatement($maData['referenceNumber'], $record);

                if ($bankStatement instanceof BankStatement) {
                    $record["duplicate"] = true;
                }

                if (!isset($parsedData[$maData['id']])) {
                    $parsedData[$maData['id']] = $maData;
                    $parsedData[$maData['id']]['new'][] = $record;
                    $pastPayments = $this->getRepository()->findMaPaymentsByReferenceNumber($maData['referenceNumber']);
                    $parsedData[$maData['id']]['past'] = $pastPayments;
                } else {
                    $parsedData[$maData['id']]['new'][] = $record;
                }

                if (isset($parsedData[$maData['id']]['past'])) {
                    $pastPayments = $parsedData[$maData['id']]['past'];

                    if ($bankStatement !== null) {
                        $needReplace = false;

                        foreach ($pastPayments as &$pastPayment) {
                            if ($pastPayment["id"] == $bankStatement->getId()) {
                                $pastPayment["duplicate"] = true;
                                $needReplace = true;
                                $parsedData[$maData['id']]["duplicate"] = true;
                            }
                        }

                        if ($needReplace) {
                            $parsedData[$maData['id']]['past'] = $pastPayments;
                        }
                    }
                }
            }
        }

        return $parsedData;
    }

    /**
     * @param $referenceNumber
     * @param array $record
     * @return null|object
     */
    protected function checkPreviousBankStatement($referenceNumber, array $record)
    {
        $date = new \DateTime();
        $date->setTimestamp(strtotime($record['date']));
        $amount = $this->parseAmountToFloat($record['amount']);

        $bankStatement = $this->getRepository()->findOneBy([
            "date" => $date,
            "amount" => $amount,
            "referenceNumber" => $referenceNumber
        ]);

        return $bankStatement;
    }

    /**
     * @param User $user
     * @param array $csvData
     * @param array $confirmIds
     * @param $importFields
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFromCSV(User $user, array $csvData, array $confirmIds, $importFields)
    {
        $row = 1;

        $sendOnlyPaymentReceivedNotifications = [];

        if (isset($confirmIds['payment_received_only'])) {
            $sendOnlyPaymentReceivedNotifications = array_keys($confirmIds['payment_received_only']);
            unset($confirmIds['payment_received_only']);
        }

        $transactionCode = (new TokenGenerator())->generateToken();

        foreach ($this->yieldArray($csvData) as $record) {
            $row++;

            if (!isset($record['id'])) {
                continue;
            }

            $memberApplication = $this->em
                ->getRepository(MemberApplication::class)
                ->findOneBy(['id' => $record['id']]);

            if (!$memberApplication instanceof MemberApplication) {
                $this->getPlatformLogService()->create(
                    sprintf('Can not find Member Application with id %s', $record['id']),
                    sprintf('Can not find Member Application with id %s. Row %s', $record['id'], $row)
                );

                continue;
            }

            foreach ($this->yieldArray($record['new']) as $bankStatement) {
                if (!in_array($bankStatement['uniqueid'], $confirmIds)) {
                    continue;
                }

                $missing = array_diff_key(array_flip($importFields), $bankStatement);

                if (count($missing) > 0) {
                    $this->getPlatformLogService()->create(
                        'Required fields are missed',
                        sprintf(
                            'Please, check required fields in file. Row %s',
                            implode(',', $importFields),
                            $row
                        )
                    );

                    continue;
                }

                if (!isset($record['id'])) {
                    $this->getPlatformLogService()->create(
                        'Can not find id of Member Application',
                        sprintf(
                            'Can not find id of Member Application. Row %s',
                            $row
                        )
                    );

                    continue;
                }

                if (strlen($bankStatement['date']) == 0) {
                    $this->getPlatformLogService()->create(
                        'Required field "date" is not found',
                        sprintf(
                            'Required field "date" is not found. Row %s',
                            $row
                        )
                    );

                    continue;
                }

                $amount = $this->parseAmountToFloat($bankStatement['amount']);

                if ($amount == 0) {
                    $this->getPlatformLogService()->create(
                        'Required field "amount" is not found or incorrect',
                        sprintf(
                            'Required field "amount" is not found or incorrect. Row %s',
                            $row
                        )
                    );

                    continue;
                }

                $previousBSStatuses = $this->getRepository()->getStatusesOfPreviousRecords(
                    $memberApplication,
                    $transactionCode
                );

                $paymentDescription = $bankStatement['text'];

                $date = new \DateTime();
                $date->setTimestamp(strtotime($bankStatement['date']));

                $now = new \DateTime();
                $typeLabel = 'CSV UPLOAD ' . $now->format('Y-m-d H:i:s');

                $bankStatementEntity = new BankStatement();

                $bankStatementEntity->setAmount($amount)
                    ->setCreatedAt($now)
                    ->setPaymentDescription($paymentDescription)
                    ->setCreatedBy($user->getName())
                    ->setDate($date)
                    ->setMember($memberApplication->getMember())
                    ->setMemberApplication($memberApplication)
                    ->setOffer($memberApplication->getOffer())
                    ->setReferenceNumber($record['referenceNumber'])
                    ->setType(BankStatement::CSV_TYPE)
                    ->setTypeLabel($typeLabel)
                    ->setUniqueId($bankStatement['uniqueid'])
                    ->setTransactionCode($transactionCode);

                $pastBankPayments = floatval($this->getRepository()->calculatePaymentsForMa($memberApplication));
                $needToPay = floatval($memberApplication->getNumberOfShares() * $memberApplication->getPricePerShare());

                $amountWithCurrentPayment = $pastBankPayments + $amount;

                list($bsStatus, $maStatus, $listener) = $this->checkBalance($amountWithCurrentPayment, $needToPay);

                $bankStatementEntity->setStatus($bsStatus);
                $memberApplication->setStatus($maStatus);

                $this->em->persist($bankStatementEntity);
                $this->em->flush();

                $sendNotification = true;

                if (!empty($sendOnlyPaymentReceivedNotifications) &&
                    in_array($memberApplication->getId(), $sendOnlyPaymentReceivedNotifications) &&
                    ($memberApplication->getStatus() !== MemberApplication::PAYMENT_RECEIVED &&
                        $memberApplication->getStatus() !== MemberApplication::OVERPAYMENT_RECEIVED
                    )
                ) {
                    $sendNotification = false;
                } elseif (in_array($bsStatus, $previousBSStatuses)) {
                    $sendNotification = false;
                }

                if ($sendNotification) {
                    $this->notificationRoutine($memberApplication, $listener);
                }
            }
        }
    }

    /**
     * @param $amountWithCurrentPayment
     * @param $needToPay
     * @return array
     */
    public function checkBalance($amountWithCurrentPayment, $needToPay)
    {
        if ($amountWithCurrentPayment == $needToPay) {
            $bsStatus = BankStatement::PAYMENT_RECEIVED;
            $maStatus = MemberApplication::PAYMENT_RECEIVED;
            $listener = NotificationListeners::MEMBER_APPLICATION_PAYMENT_RECEIVED;
        } elseif ($amountWithCurrentPayment > $needToPay) {
            $bsStatus = BankStatement::OVERPAYMENT_RECEIVED;
            $maStatus = MemberApplication::OVERPAYMENT_RECEIVED;
            $listener = NotificationListeners::MEMBER_APPLICATION_OVERPAYMENT;
        } else {
            $bsStatus = BankStatement::PART_PAYMENT_RECEIVED;
            $maStatus = MemberApplication::PART_PAYMENT_RECEIVED;
            $listener = NotificationListeners::MEMBER_APPLICATION_PART_PAYMENT;
        }

        return [$bsStatus, $maStatus, $listener];
    }

    /**
     * @param MemberApplication $memberApplication
     * @param $listener
     */
    public function notificationRoutine(MemberApplication $memberApplication, $listener)
    {
        $this->getNotification()->sendViaEnabledMethodsByListenerViaRabbitMQ(
            $listener,
            $memberApplication->getMember(),
            [
                'memberApplication' => $memberApplication,
                'user' => $memberApplication->getMember(),
                'offer' => $memberApplication->getOffer(),
                'application' => $memberApplication->getOffer()->getApplication(),
            ]
        );

        $this->getMemberApplicationNotifier()->sendStatusMessage($memberApplication);
    }

    /**
     * @param $num
     * @return float
     */
    protected function parseAmountToFloat($num)
    {
        if ($num[0] === "$") {
            $num = substr($num, 1);
        }

        $withoutComma = str_replace(",", "", $num);

        return floatval($withoutComma);
    }

    /**
     * @param MemberApplication $memberApplication
     * @param array $new
     * @return array
     */
    public function getDataForAjaxRow(MemberApplication $memberApplication, array $new)
    {
        /** @var Offer $offer */
        $offer = $memberApplication->getOffer();

        $pastPayments = $this->getRepository()
            ->findMaPaymentsByReferenceNumber($memberApplication->getReferenceNumber())
        ;

        return [
            "id" => $memberApplication->getId(),
            "referenceNumber" => $memberApplication->getReferenceNumber(),
            "applicant1Investor" => $memberApplication->getApplicant1Investor(),
            "pricePerShare" => $memberApplication->getPricePerShare(),
            "numberOfShares" => $memberApplication->getNumberOfShares(),
            "status" => $memberApplication->getStatus(),
            "offer_id" => $offer->getId(),
            "offer_title" => $offer->getTitle(),
            "clear_status" => MemberApplication::convertMAStatus($memberApplication->getStatus()),
            "new" => [
                $new
            ],
            "past" => $pastPayments
        ];
    }

    /**
     * @param array $array
     * @return \Generator
     */
    public function yieldArray(array $array)
    {
        foreach ($array as $current) {
            yield $current;
        }
    }
}
